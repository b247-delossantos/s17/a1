/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function generalInfo(){
		let fullName = prompt('What is your name?');
		let Age = prompt('How old are you?');
		let location = prompt('Where do you live?')

		console.log('Hello, '+ fullName);
		console.log('You are '+ Age + ' years old.');
		console.log('You live in ' + location);
		alert('Thank you for your input.')
;	}

	generalInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBands(){
		let firstBand = ('1. The Beatles');
		let secondBand = ('2. Metallica');
		let thirdBand = ('3. The Eagles');
		let fourthBand = ("4. L'~arc~en~Ciel");
		let lastBand = ('5. Eraserheads');

		console.log(firstBand);
		console.log(secondBand);
		console.log(thirdBand);
		console.log(fourthBand);
		console.log(lastBand);

	};

	favoriteBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies(){
		let firstMovie = ('1. The Da Vinci Code');
		let ratingFirst = ("25%");
		let secondMovie = ('2. Inferno');
		let ratingSecond = ("23%");
		let thirdMovie = ('3. Angels & Demons');
		let ratingThird = ("37%");
		let fourthMovie = ("4. Peaky Blinders");
		let ratingFourth = ("93%");
		let lastMovie = ('5. Shutter Island');
		let ratingLast = ("68%");

		console.log(firstMovie);
		console.log('Rotten Toamtoes Rating: '+ ratingFirst);
		console.log(secondMovie);
		console.log('Rotten Toamtoes Rating: '+ ratingSecond);
		console.log(thirdMovie);
		console.log('Rotten Toamtoes Rating: '+ ratingThird);
		console.log(fourthMovie);
		console.log('Rotten Toamtoes Rating: '+ ratingFourth);
		console.log(lastMovie);
		console.log('Rotten Toamtoes Rating: '+ ratingFourth);


	};

	favoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

	

};

printUsers();

/*console.log(friend1);
console.log(friend2);*/